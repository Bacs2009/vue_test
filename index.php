<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
<script src="https://use.fontawesome.com/f4610f30a9.js"></script>

<link rel="stylesheet" href="styles.css">

<div id="app">
    <div class="container">
        <div class="comments-outside">
            <div class="comments-header">
                <div class="row">
                    <div class="col-md-6">
                        <div class="comments-stats">
                            <span><i class="fa fa-thumbs-up"></i> [[ likes ]]</span>
                            <span><i class="fa fa-comment"></i> [[ comments.length ]]</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="project-owner pull-right">
                            <div class="avatar">
                                <img :src="creator.avatar" alt="">
                            </div>
                            <div class="username">
                                <a href="#">@[[ creator.user ]]</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <comments 
            :comments_wrapper_classes="['custom-scrollbar', 'comments-wrapper']"
            :comments="comments"
            :current_user="current_user"
            @submit-comment="submitComment"
            ></comments>
        </div>
        <div v-if="showModal" @close="showModal = false" id="modal-template">
            <h3 slot="header">Edit your comment</h3>
            <transition name="modal">
            <div class="modal-mask">
              <div class="modal-wrapper">
                <div class="modal-container">

                  <div class="modal-header">
                    <slot name="header">
                      Edit your comment
                    </slot>
                  </div>

                  <div class="modal-body">
                    <slot name="body">
                    <input 
                        type="hidden" 
                        v-model.trim="cid" 
                    />
                    <input 
                        type="text" 
                        v-model.trim="edit" 
                        class="reply--text form-control" 
                        placeholder="Leave a comment..."
                        maxlength="250"
                        required
                        @keyup.enter="submitCommentWithID(edit, cid)"
                    />
                    </slot>
                  </div>

                  <div class="modal-footer">
                    <slot name="footer">
                      
                        <button class="modal-default-button" @click.prevent="submitCommentWithID(edit, cid)">
                        OK
                        </button>
                    </slot>
                  </div>
                </div>
              </div>
            </div>
          </transition>
        </div>
    </div>
</div>

<script src="app.js"></script>