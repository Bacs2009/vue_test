function uuid() {
    var uuid = "",
        i, random;
    for (i = 0; i < 32; i++) {
        random = Math.random() * 16 | 0;

        if (i == 8 || i == 12 || i == 16 || i == 20) {
            uuid += "-"
        }
        uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
    }
    return uuid;
}

Vue.component('single-comment', {
    delimiters: ['[[', ']]'],
    template: `
        
                        
            <div class="comment">
                <div class="row">
                    <div class="col-md-1">
                        <div class="avatar">
                            <img :src="comment.avatar" alt="">
                        </div>
                        <p id="cid" style="display:none;">[[ comment.id ]]</p>
                        <a class="username" href="#">
                            @[[ comment.user ]]
                        </a> 
                    </div>
                    <div class="col-md-7">
                        <span>[[ comment.text ]]</span>
                    </div>
                    <div class="col-md-1">
                        <span class='right floated edit icon' id="show-modal" @click="showM([[ comment.id ]])">
                            <i class='fa fa-edit fa-2x'></i>
                        </span>
                    </div>
                </div>
                <hr>
            </div>

        
    `,
    props: ['comment'],
    methods: {
        showM: function(cid) {
            // alert(cid);
            var obj = null;
            for (var i = 0; i < app.comments.length; i++) {
                if (app.comments[i].id == cid) {
                    obj = app.comments[i];
                    break;
                }
            }
            // console.log(obj.text);
            app.shs = obj.text;
            app.cid = cid;
            app.showModal = true;
            app.edit = obj.text
        }
    }
});

Vue.component('comments', {
    delimiters: ['[[', ']]'],
    template: `
        <div class="comments">
            <div :class="comments_wrapper_classes">
                <single-comment 
                    v-for="comment in comments"
                    :comment="comment"
                    :key="comment.id"
                ></single-comment>
            </div>
            <div class="reply">
                <div class="row">
                    <div class="col-md-4">
                        <input 
                            type="text" 
                            v-model.trim="n" 
                            class="reply--text form-control" 
                            placeholder="Enter your name..."
                            maxlength="15"
                            required
                            @keyup.enter="submitComment"
                        />
                    </div>
                    <div class="col-md-6">
                        <input 
                            type="text" 
                            v-model.trim="reply" 
                            class="reply--text form-control" 
                            placeholder="Leave a comment..."
                            maxlength="250"
                            required
                            @keyup.enter="submitComment"
                        />
                    </div>
                    <div class="col-md-2">    
                        <button 
                            class="reply--button btn btn-success" 
                            @click.prevent="submitComment">
                            <i class="fa fa-paper-plane"></i> Send
                        </button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data: function() {
        return {
            reply: '',
            n: ''
        }
    },
    methods: {
        //Tell the parent component(main app) that we have a new comment
        submitComment: function() {
            if (this.reply != '' && this.n != '') {
                this.$emit('submit-comment', this.reply, this.n);
                this.reply = '';
                this.n = '';
            }
        }
    },
    //What the component expects as parameters
    props: ['comments', 'current_user', 'comments_wrapper_classes']
});

// register modal component
// var mod = Vue.component('modal', {
//     template: '#modal-template',
//     data: function() {
//         return {
//             showModal: 'Text'
//         }
//     },
// });

const app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: function() {
        return {
            shs: 'Ex',
            cid: '22',
            showModal: false,
            likes: 12,
            //Info about the owner of the post
            creator: {
                avatar: 'https://ui-avatars.com/api/?name=John+Doe',
                user: 'owner'
            },
            //Some info about the current user
            current_user: {
                avatar: 'https://ui-avatars.com/api/?name=Mike+Kein',
                user: 'exampler'
            },
            //Comments that are under the post
            comments: [{
                id: uuid(),
                user: 'example',
                avatar: 'https://ui-avatars.com/api/?name=John+Doe',
                text: 'Leave your comment here',
            }, ]
        }
    },
    props: ['edit'],
    methods: {
        submitComment: function(reply, n) {
            this.comments.push({
                id: uuid(),
                user: n,
                avatar: 'https://ui-avatars.com/api/?name=' + n,
                text: reply
            });
        },
        submitCommentWithID: function(edit, cid) {
            for (var i = 0; i < this.comments.length; i++) {
                if (this.comments[i].id == cid[0][0]) {
                    this.comments[i].text = edit;
                    break;
                }
            }
            this.showModal = false;
        }

    }
});